
const express = require('express'),
app = express(),
redis = require('redis'),
client = redis.createClient(6379),
dotenv = require('dotenv').config();

try{
	client.on('error', function(err){
    console.log('Error ' + err);
	});

	app.set('port', process.env.PORT || 8080);

	const server = app.listen(app.get('port'), function(){
	  	console.log('##########################################################');
	  	console.log('#####               STARTING SERVER                  #####');
	  	console.log('##########################################################\n');
	  	console.log('Express running on PORT',process.env.PORT);
	});
}catch(err){
	console.log(err);
}
